package com.example.booking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.booking.Interface.ItemClickListner;
import com.example.booking.Model.Category;
import com.example.booking.Model.Food;
import com.example.booking.ViewHolder.FoodViewHolder;
import com.example.booking.ViewHolder.MenuViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class FoodList extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference foodlist;
    RecyclerView recycler_menu;
    RecyclerView.LayoutManager layoutManager;

    String categoryId = "";

    FirebaseRecyclerAdapter<Food, FoodViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);


        //Init Firebase
        database = FirebaseDatabase.getInstance();
        foodlist = database.getReference("Food");


        recycler_menu = (RecyclerView)findViewById(R.id.recycler_menu);
        recycler_menu.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);

        recycler_menu.setLayoutManager(layoutManager);

        if (getIntent() != null) {

            categoryId = getIntent().getStringExtra("CategoryID");
            if (!categoryId.isEmpty() && categoryId != null) {
                loadListFood(categoryId);
            }
        }
    }

    private void loadListFood(String catId) {
        FirebaseRecyclerOptions<Food> options;
        options = new FirebaseRecyclerOptions.Builder<Food>().setQuery(foodlist.orderByChild("MenuId").equalTo(catId), Food.class).build();
        adapter = new FirebaseRecyclerAdapter<Food, FoodViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FoodViewHolder foodViewHolder, int i, @NonNull Food food) {

                //Render Image using Picasso module
                Picasso.get().load(food.getImage()).fit().into(foodViewHolder.image_view);
                foodViewHolder.food_text.setText(food.getName());
                final Food clickItem = food;
                foodViewHolder.setItemClickListener(new ItemClickListner() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Toast.makeText(FoodList.this, "" + clickItem.getName(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_item, parent, false);
                return new FoodViewHolder(view);
            }
        };
        recycler_menu.setLayoutManager(layoutManager);
        adapter.startListening();
        recycler_menu.setAdapter(adapter);
    }
}
