package com.example.booking.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.booking.Interface.ItemClickListner;
import com.example.booking.R;

public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView food_text;
    public ImageView image_view;

    private ItemClickListner itemClickListener;

    public FoodViewHolder(@NonNull View itemView, ItemClickListner itemClickListner) {
        super(itemView);
        this.itemClickListener = itemClickListner;
    }

    public FoodViewHolder(@NonNull View itemView) {
        super(itemView);

        food_text = (TextView)itemView.findViewById(R.id.food_text);
        image_view = (ImageView)itemView.findViewById(R.id.food_image);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListner itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {

    }
}
